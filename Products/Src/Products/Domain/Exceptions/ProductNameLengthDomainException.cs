﻿using System;
using dotnet_monorepo_clean_architecture.Shared.Domain;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Exceptions
{
    public class ProductNameLengthDomainException : DomainException
    {
        public ProductNameLengthDomainException() : base("Product Name Length Exception", "PRODUCT_NAME_LENGTH") { }
    }
}
