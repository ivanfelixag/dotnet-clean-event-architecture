﻿using System;
using System.Collections;
using System.Collections.Generic;
using dotnet_monorepo_clean_architecture.Shared.Domain.Bus.Event;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Events
{
    public class ProductSearchedDomainEvent : DomainEvent
    {
        private String _EventName = "products.1.event.product.searched";
        private String Id;
        private String Name;
        private String Description;

        public ProductSearchedDomainEvent(String Id, String Name, String Description, String EventId = null, String OcurredOn = null)
            : base(Id, EventId, OcurredOn)
        {
            this.Id = Id;
            this.Name = Name;
            this.Description = Description;
        }

        public override string EventName()
        {
            return this._EventName;
        }

        public override DomainEvent FromPrimitives(string aggregateId, Hashtable body, string eventId, string occurredOn)
        {
            return new ProductCreatedDomainEvent(aggregateId, body["name"].ToString(), body["description"].ToString(), eventId, occurredOn);
        }

        public override Dictionary<string, Object> ToPrimitives()
        {
            return new Dictionary<string, Object>(){
                {"id", this.Id},
                {"name", this.Name},
                {"description", this.Description + " - Searched"}
            };
        }
    }
}
