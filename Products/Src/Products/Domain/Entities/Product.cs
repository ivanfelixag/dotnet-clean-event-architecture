﻿using System;
namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String SKU { get; set; }

        public Product() { }

        public Product(Guid Id, String Name, String Description, String SKU)
        {
            this.Id = Id;
            this.Name = Name;
            this.Description = Description;
            this.SKU = SKU;
        }
    }
}
