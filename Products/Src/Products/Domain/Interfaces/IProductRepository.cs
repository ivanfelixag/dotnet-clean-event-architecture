﻿using System;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Interfaces
{
    public interface IProductRepository
    {
        public Product Save(Product product);

        public Product SearchById(String id);
    }
}
