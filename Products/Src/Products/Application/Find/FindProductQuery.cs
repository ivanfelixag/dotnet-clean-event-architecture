﻿using System;
using MediatR;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create
{
    public class FindProductQuery : IRequest<FindProductQueryResponse>
    {
        public String Id { get; set; }
    }
}
