﻿using System;
using System.Threading;
using System.Threading.Tasks;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Events;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Exceptions;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Interfaces;
using dotnet_monorepo_clean_architecture.Shared.Domain.Bus.Event;
using MediatR;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create
{
    public class FindProductQueryHandler : IRequestHandler<FindProductQuery, FindProductQueryResponse>
    {
        private IProductRepository Repository;
        private IEventBus EventBus;
        public FindProductQueryHandler(IProductRepository Repository, IEventBus EventBus)
        {
            this.Repository = Repository;
            this.EventBus = EventBus;
        }

        public async Task<FindProductQueryResponse> Handle(FindProductQuery request, CancellationToken cancellationToken)
        {
            Product Product = this.Repository.SearchById(request.Id);

            this.EventBus.Publish(new ProductSearchedDomainEvent(Product.Id.ToString(), Product.Name, Product.Description));

            return new FindProductQueryResponse
            {
                Product = Product
            };
        }
    }
}
