﻿using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create
{
    public class FindProductQueryResponse
    {
        public Product Product { get; set; }
    }
}
