﻿using System;
using MediatR;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create
{
    public class CreateProductCommand : IRequest<CreateProductCommandResponse>
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String SKU { get; set; }
    }
}
