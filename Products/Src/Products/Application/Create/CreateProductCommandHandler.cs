﻿using System;
using System.Threading;
using System.Threading.Tasks;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Exceptions;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Interfaces;
using dotnet_monorepo_clean_architecture.Shared.Domain.Bus.Event;
using MediatR;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Events;

namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, CreateProductCommandResponse>
    {
        private IProductRepository Repository;
        private IEventBus EventBus;
        public CreateProductCommandHandler(IProductRepository Repository, IEventBus EventBus)
        {
            this.Repository = Repository;
            this.EventBus = EventBus;
        }

        public async Task<CreateProductCommandResponse> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            if(request.Name.Length > 30)
            {
                throw new ProductNameLengthDomainException();
            }

            Product Product = new Product(Guid.NewGuid(), request.Name, request.Description, request.SKU);

            this.Repository.Save(Product);

            this.EventBus.Publish(new ProductCreatedDomainEvent(Product.Id.ToString(), Product.Name, Product.Description));

            return new CreateProductCommandResponse
            {
                Product = Product
            };
        }
    }
}
