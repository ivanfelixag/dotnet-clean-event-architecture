﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
namespace dotnet_monorepo_clean_architecture.Products.Src.Products.Infrastucture.Models
{
    public class ProductModel : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products"); // Plural
            
            builder.Property(p => p.Id).IsRequired();

            builder.Property(p => p.Name).IsRequired().HasMaxLength(30);

            builder.Property(p => p.Description).IsRequired();

            builder.Property(p => p.SKU).IsRequired();

            builder.HasKey(p => p.Id);
        }
    }
}
