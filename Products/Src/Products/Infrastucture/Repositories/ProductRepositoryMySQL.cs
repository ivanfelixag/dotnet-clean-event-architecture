﻿using System;
using System.Linq;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Interfaces;
using dotnet_monorepo_clean_architecture.Products.Shared.Infrastructure.MySQL;

namespace dotnet_monorepo_clean_architecture.src.products.products.infrastucture.repositories
{
    public class ProductRepositoryMySQL : EfRepository, IProductRepository
    {
        public ProductRepositoryMySQL(AppDbContext dbContext) : base(dbContext)
        {
        }

        public Product Save(Product product)
        {
            this.dbContext.Set<Product>().Add(product);
            this.dbContext.SaveChanges();

            return product;
        }

        public Product SearchById(String id)
        {
            return this.dbContext.Set<Product>().FirstOrDefault(e => e.Id.ToString() == id);
        }
    }
}
