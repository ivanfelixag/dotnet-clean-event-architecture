﻿using System;
using System.Reflection;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Domain.Entities;
using Microsoft.EntityFrameworkCore;
namespace dotnet_monorepo_clean_architecture.Products.Shared.Infrastructure.MySQL
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Added SKU is unique constrait using the Fluent API
            modelBuilder.Entity<Product>().HasIndex(u => u.SKU).IsUnique();

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetEntryAssembly());
        }
    }
}