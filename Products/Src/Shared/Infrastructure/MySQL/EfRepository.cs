﻿using System;
using dotnet_monorepo_clean_architecture.Shared.Domain;
namespace dotnet_monorepo_clean_architecture.Products.Shared.Infrastructure.MySQL
{
    public class EfRepository : IRepository
    {
        protected readonly AppDbContext dbContext;

        public EfRepository(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
