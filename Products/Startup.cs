using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using dotnet_monorepo_clean_architecture.Products.Shared.Infrastructure.MySQL;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System.Reflection;
using dotnet_monorepo_clean_architecture.Products;
using MediatR;
using Amazon.SimpleNotificationService;

namespace Products
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<AppDbContext>(options =>
                options.UseMySql(Configuration.GetValue<string>("ConnectionStrings:DefaultConnection"), mySqlOptions => mySqlOptions
                    // replace with your Server Version and Type
                    .ServerVersion(new Version(8, 0, 18), ServerType.MySql))
            );

            // Mediator 
            services.AddMediatR(Assembly.GetExecutingAssembly());

            // AWS SDK Configurattion
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());

            // AWS SNS
            services.AddAWSService<IAmazonSimpleNotificationService>();

            return ContainerSetup.InitializeWeb(Assembly.GetExecutingAssembly(), services);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDbContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
