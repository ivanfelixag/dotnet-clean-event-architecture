﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Infrastucture.Models;
using dotnet_monorepo_clean_architecture.Shared.Infrastructure.Bus.Event.AWS;

namespace dotnet_monorepo_clean_architecture.Products
{
    public static class ContainerSetup
    {
        public static IServiceProvider InitializeWeb(Assembly webAssembly, IServiceCollection services) =>
            new AutofacServiceProvider(BaseAutofacInitialization(setupAction =>
            {
                setupAction.Populate(services);
                setupAction.RegisterAssemblyTypes(webAssembly).AsImplementedInterfaces();
            }));

        public static IContainer BaseAutofacInitialization(Action<ContainerBuilder> setupAction = null)
        {
            var builder = new ContainerBuilder();

            var modelsAssemblyTypes = Assembly.GetAssembly(typeof(ProductModel));
            var sharedAssemblyTypes = Assembly.GetAssembly(typeof(AwsSnsEventBus));
            builder.RegisterAssemblyTypes(modelsAssemblyTypes, sharedAssemblyTypes).AsImplementedInterfaces();

            setupAction?.Invoke(builder);
            return builder.Build();
        }
    }
}
