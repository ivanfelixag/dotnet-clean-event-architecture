﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnet_monorepo_clean_architecture.Products.Src.Products.Application.Create;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Products.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator mediator;
        public ProductsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody] CreateProductCommand command)
        {
            var response = await mediator.Send(command);

            return Ok(response);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> FindProduct(String id)
        {
            FindProductQuery query = new FindProductQuery();
            query.Id = id;

            var response = await mediator.Send(query);

            return Ok(response);
        }
    }
}
