﻿using System;
using System.Threading;
using System.Threading.Tasks;
using dotnet_monorepo_clean_architecture.Shared.Domain.Bus.Event;
using MediatR;

namespace dotnet_monorepo_clean_architecture.Mailer.Src.Mails.Application.SendNotificationToAdministrators
{
    public class SendNotificationToAdministratorsCommandHandler : IRequestHandler<SendNotificationToAdministratorsCommand, bool>
    {
        private IEventBus EventBus;
        public SendNotificationToAdministratorsCommandHandler(IEventBus EventBus)
        {
            this.EventBus = EventBus;
        }

        public async Task<bool> Handle(SendNotificationToAdministratorsCommand request, CancellationToken cancellationToken)
        {
            // Send mail
            return true;
        }
    }
}
