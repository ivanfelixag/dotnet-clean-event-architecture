﻿using System;
using MediatR;

namespace dotnet_monorepo_clean_architecture.Mailer.Src.Mails.Application.SendNotificationToAdministrators
{
    public class SendNotificationToAdministratorsCommand : IRequest<bool>
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}
