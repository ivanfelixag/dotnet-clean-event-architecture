﻿using System;
namespace dotnet_monorepo_clean_architecture.Shared.Domain
{
    public class DomainException : Exception
    {
        protected String message;
        protected String errorCode;

        public DomainException(String message, String errorCode = null) : base(message) {
            this.message = message;
            this.errorCode = errorCode;
        }

        public String Message()
        {
            return this.message;
        }

        public String ErrorCode()
        {
            return this.errorCode;
        }
    }
}
