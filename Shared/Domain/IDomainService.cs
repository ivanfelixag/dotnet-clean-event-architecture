﻿using System;
namespace Shared.Domain
{
    public interface IDomainService<InputType, OutputType>
    {
        public OutputType Execute(InputType input);
    }
}
