﻿using System;
namespace dotnet_monorepo_clean_architecture.Shared.Domain.Bus.@Event
{
    public interface IDomainEventSuscriber
    {

        public abstract Array SuscribedTo();

    }
}
