﻿using System;
namespace dotnet_monorepo_clean_architecture.Shared.Domain.Bus.@Event
{
    public interface IEventBus
    {

        public void Publish(DomainEvent _event);

        public void Publish(DomainEvent[] events);

    }
}
