﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace dotnet_monorepo_clean_architecture.Shared.Domain.Bus.@Event
{
    public abstract class DomainEvent
    {
        protected String AggregateId { get; private set; }
        protected String EventId { get; private set; }
        protected String OcurredOn { get; private set; }

        public DomainEvent(String AggegateId)
        {
            this.AggregateId = AggregateId;
            this.EventId = Guid.NewGuid().ToString();
            this.OcurredOn = DateTime.Now.ToString();
        }

        public DomainEvent(String AggegateId, String EventId, String OcurredOn)
        {
            this.AggregateId = AggregateId;
            this.EventId = EventId is null ? Guid.NewGuid().ToString() : EventId;
            this.OcurredOn = OcurredOn is null ? DateTime.Now.ToString() : OcurredOn;
        }

        public abstract DomainEvent FromPrimitives(String aggregateId, Hashtable body, String eventId, String occurredOn);

        public abstract String EventName();

        public abstract Dictionary<string, Object> ToPrimitives();
    }
}
