﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using dotnet_monorepo_clean_architecture.Shared.Domain.Bus.Event;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Binder;
using Newtonsoft.Json;

namespace dotnet_monorepo_clean_architecture.Shared.Infrastructure.Bus.Event.AWS
{
    public class AwsSnsEventBus : IEventBus
    {
        private IAmazonSimpleNotificationService SimpleNotificationService;
        private IConfiguration Configuration;

        public AwsSnsEventBus(IAmazonSimpleNotificationService SimpleNotificationService, IConfiguration Configuration)
        {
            this.SimpleNotificationService = SimpleNotificationService;
            this.Configuration = Configuration;
        }

        public async void Publish(DomainEvent _event)
        {
            this.Publish(new[] { _event });
        }

        public async void Publish(DomainEvent[] events)
        {
            foreach (DomainEvent element in events)
            {
                // Step 1. Set Routing Key - "type" Filter
                var messageAttributes = new Dictionary<string, MessageAttributeValue>();
                var EventType = new MessageAttributeValue
                {
                    DataType = "String",
                    StringValue = element.EventName()
                };
                messageAttributes.Add("type", EventType);

                // Step 2. Generate the message
                var request = new PublishRequest
                {
                    Message = JsonConvert.SerializeObject(element.ToPrimitives()),
                    TopicArn = this.Configuration.GetValue<string>("AWS:TopicArn"),
                    MessageAttributes = messageAttributes
                };

                try
                {
                    // Step 3. Publish
                    var response = await this.SimpleNotificationService.PublishAsync(request);

                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        var b = 2; // Logger
                    }
                }
                catch (AmazonSimpleNotificationServiceException ex)
                {
                    var c = 2; // Logger
                }
                catch (Exception ex)
                {
                    var d = 2; // Logger
                }
            }
        }
    }
}
